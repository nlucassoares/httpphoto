Photo http player
=================

Introduction
------------

This server has for goal to list all jpg from a certain folder, to build miniature for it,
and then to display a simple page where all images can be accessed from.

To use it, run as root to be able to bind port 443, then access **https://HOSTNAME/?key=ACCESS_KEY**

Where HOSTNAME and ACCESS_KEY are variables set in configuration file.

Dependencies
------------

```bash
go get github.com/nfnt/resize
```

Compile
-------

To compile,

```bash
go build
```

To cross compile for raspbian,

```bash
env GOOS=linux GOARCH=arm GOARM=5 go build
```

Configuration
-------------

Here is a configuration example

```json
{
	"photoPath": "/home/n/Windows/Lucas/Photos/iphone",
	"cachePath": "/tmp/test",
	"certificatePath": "./cert.pem",
	"privateKeyPath": "./key.pem",
	"accessKey": "Photos",
	"hostname": "ghostbutler.ddns.net"
}
```

|Name|Type|Description|
|----|----|-----------|
|`photoPath`|string|This is the path where jpg photo will be found|
|`cachePath`|string|This is where photos miniatures will be stored|
|`certificatePath`|string|This is the SSL certificate file location|
|`privateKeyPath`|string|This is the SSL private key file location|
|`accessKey`|string|This is the access key to provide to be able to access the content|
|`hostname`|string|This is the hostname on which service is published|

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/NLucasSoares/httpphoto.git
