package core

import (
	"fmt"
	"github.com/nfnt/resize"
	"html/template"
	"image/jpeg"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const (
	ResizeImageWidth = 200

	HTMLPage = `<html>
	<head>
		<style>
			div {
				text-align: justify;
			}

			div img {
				display: inline-block;
				width: 100px;
				height: 100px;
			}

			div:after {
				content: '';
				display: inline-block;
				width: 100%;
			}
		</style>
	</head>
	<body>
		<div>
			{{ $root := . }}
			{{ range $key, $image := .PictureList }}
			<a href="https://{{ $root.Configuration.Hostname }}?key={{ $root.Configuration.AccessKey }}&photo={{ $key }}"><img src="https://{{ $root.Configuration.Hostname }}?key={{ $root.Configuration.AccessKey }}&miniature={{ $key }}" alt="{{ $key }}" /></a>
			{{ end }}
		</div>
	</body>
</html>`
)

type Image struct {
	// file path
	FilePath string

	// miniature path
	MiniaturePath string
}

type Core struct {
	// configuration
	Configuration *Configuration

	// picture list
	PictureList map[string]*Image
}

// close
func Close(closer io.ReadCloser) {
	_ = closer.Close()
}

// build core
func BuildCore(configuration *Configuration) (*Core, error) {
	// core
	core := &Core{
		Configuration: configuration,
		PictureList:   make(map[string]*Image),
	}

	// build temporary folder
	_ = os.MkdirAll(configuration.CachePath,
		0774)

	// list pictures
	if fileList, err := ioutil.ReadDir(configuration.PhotoPath); err == nil {
		count := 0
		for _, file := range fileList {
			count++
			if strings.HasSuffix(strings.ToLower(file.Name()),
				".jpg") {
				fmt.Println("Now processing",
					file.Name(),
					"(",
					strconv.FormatFloat(float64(count)/float64(len(fileList))*100.0,
						'f',
						2,
						64),
					"% )")
				if _, err := os.Stat(configuration.CachePath +
					"miniature_" +
					file.Name()); os.IsNotExist(err) {
					// open file
					if handle, err := os.Open(configuration.PhotoPath +
						file.Name()); err == nil {
						// decode image
						if img, err := jpeg.Decode(handle); err == nil {
							// close file
							Close(handle)

							// resize image
							resizeImage := resize.Resize(ResizeImageWidth,
								0,
								img,
								resize.NearestNeighbor)

							// create output file
							if out, err := os.Create(configuration.CachePath +
								"miniature_" +
								file.Name()); err == nil {
								// write image
								_ = jpeg.Encode(out,
									resizeImage,
									nil)

								// close output file
								Close(out)
							} else {
								return nil, err
							}
						} else {
							// close file
							Close(handle)

							// quit
							return nil, err
						}
					} else {
						return nil, err
					}
				}

				// build key
				key := strings.TrimRight(strings.ToLower(file.Name()),
					".jpg")

				// add picture
				core.PictureList[key] = &Image{
					FilePath: configuration.PhotoPath +
						file.Name(),
					MiniaturePath: configuration.CachePath +
						"miniature_" +
						file.Name(),
				}
			} else {
				fmt.Println("Ignoring",
					file.Name(),
					"(",
					strconv.FormatFloat(float64(count)/float64(len(fileList))*100.0,
						'g',
						2,
						64),
					"% )")
			}
		}
	} else {
		return nil, err
	}

	// done
	return core, nil
}

// run
func (core *Core) Run() {
	if err := http.ListenAndServeTLS(":443",
		core.Configuration.CertificatePath,
		core.Configuration.PrivateKeyPath,
		core); err != nil {
		fmt.Println(err)
	}
}

// serve http (implements http.Handler)
func (core *Core) ServeHTTP(rw http.ResponseWriter,
	request *http.Request) {
	// parse get form
	if err := request.ParseForm(); err == nil {
		// get key
		if valueList := request.Form["key"]; valueList != nil &&
			len(valueList) > 0 {
			// check key
			if valueList[0] == core.Configuration.AccessKey {
				if valueList = request.Form["photo"]; valueList != nil &&
					len(valueList) > 0 {
					if file, ok := core.PictureList[valueList[0]]; ok {
						// read file
						if fileContent, err := ioutil.ReadFile(file.FilePath); err == nil {
							rw.Header().Add("Content-Type",
								"image/jpeg")
							_, _ = rw.Write(fileContent)
						}
					} else {
						fmt.Println("can't open photo (" +
							valueList[0] +
							")")
						rw.WriteHeader(http.StatusBadRequest)
					}
				} else {
					if valueList = request.Form["miniature"]; valueList != nil &&
						len(valueList) > 0 {
						if file, ok := core.PictureList[valueList[0]]; ok {
							// read file
							if fileContent, err := ioutil.ReadFile(file.MiniaturePath); err == nil {
								rw.Header().Add("Content-Type",
									"image/jpeg")
								_, _ = rw.Write(fileContent)
							}
						} else {
							fmt.Println("can't open miniature (" +
								valueList[0] +
								")")
							rw.WriteHeader(http.StatusBadRequest)
						}
					} else {
						// create html template
						html := template.New("page")

						// parse page
						html, _ = html.Parse(HTMLPage)

						// write to stream
						if err = html.Execute(rw,
							core); err != nil {
							fmt.Println(err)
							rw.WriteHeader(http.StatusInternalServerError)
						}
					}
				}
			} else {
				fmt.Println("Bad access key ("+
					valueList[0],
					")")
				rw.WriteHeader(http.StatusBadRequest)
			}
		} else {
			fmt.Println("Invalid request (no access key)")
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte("Nop"))
		}
	} else {
		fmt.Println("can't parse form:",
			err)
		rw.WriteHeader(http.StatusBadRequest)
	}
}
